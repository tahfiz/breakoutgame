﻿class BreakoutGame {

    left: Phaser.Key
    right: Phaser.Key
    paddle: Phaser.Sprite
    brick: Phaser.Sprite
    bricks: Phaser.Group
    ball: Phaser.Sprite

    constructor() {
        this.game = new Phaser.Game(420, 450, Phaser.AUTO, 'content', { create: this.create, preload: this.preload, update: this.update, hit: this.hit});
    }

    game: Phaser.Game;

    preload() {
        //Graphics
        this.game.load.image('paddle', '/Graphics/paddle.png');
        this.game.load.image('brick', '/Graphics/brick.png');
        this.game.load.image('ball', '/Graphics/ball.png');

        //Sounds
        this.game.load.audio('BGM', ['/Sounds/BGMsound.mp3', '/Sounds/BGMsound.ogg']);
    }

    create() {
        //BGM
        var bgm = this.game.add.audio('BGM');
        bgm.play();

        //Background image and text
        var text = "BREAKOUT";
        var style = { font: "65px Arial", fill: "#ff0000", align: "center" };
        this.game.add.text(0, 0, text, style);
        this.game.stage.backgroundColor = '#191970';

        //Enable Physics
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.world.enableBody = true;


        //Create left/right keys
        this.left = this.game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
        this.right = this.game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);

        //Add paddle in-game and ensure the paddle is immovable
        this.paddle = this.game.add.sprite(200, 400, 'paddle');
        this.paddle.body.immovable = true;

        //Add 5x5 bricks
        this.bricks = this.game.add.group();
        for (var i = 0; i < 5; i++) {
            for (var j = 0; j < 5; j++) {
                var brick = this.game.add.sprite(55 + i * 60, 55 + j * 35, 'brick');
                brick.body.immovable = true;
                this.bricks.add(brick);
            }
        }

        //Ball
        this.ball = this.game.add.sprite(200, 300, 'ball');
        this.ball.body.velocity.x = 200;
        this.ball.body.velocity.y = 200;

        //Make ball bounces
        this.ball.body.bounce.setTo(1);
        this.ball.body.collideWorldBounds = true;


    }

    update() {

        //Move the paddle according the key pressed
        if (this.left.isDown) this.paddle.body.velocity.x = -300;
        else if (this.right.isDown) this.paddle.body.velocity.x = 300;

        //Stop the paddle when no key is pressed
        else this.paddle.body.velocity.x = 0;

        //Collisions between paddle and ball
        this.game.physics.arcade.collide(this.paddle, this.ball);
        this.game.physics.arcade.collide(this.ball, this.bricks, this.hit, null, this);
        if (this.ball.y > this.paddle.y) {
            this.game.state.restart();
            this.game.sound.stopAll();
        }

    }

    hit(ball, brick) {
        brick.kill();
    }


}

window.onload = () => {
    var game = new BreakoutGame();

};